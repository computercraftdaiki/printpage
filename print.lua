local function getPrinterPeripheral()
	printer = peripheral.find("printer")
	if printer == nil then
		error("Error : Printer not found")
    end
    return printer
end

local function getTextFromFile(filename)
    local text
    local f = open(filename, "r")
    if f == nil then
        error("File " .. filename .. " not found")
    else
        text = f.readAll()
    end
    f.close()
    return text
end

local function print(printer, string, title)
    printer.newPage()
    printer.setPageTitle(title)
    local i = 1
    for line in string do
        printer.setCursorPos(1, i)
        printer.write(line)
    end
    printer.endPage()
end

local function main()
    printer = getPrinterPeripheral()
    text = getTextFromFile("text")
    while true do
        print(printer, text)
        sleep(5)
    end
end

main()